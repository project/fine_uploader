<?php

/**
 * @file
 * CCK widget file.
 */

module_load_include('inc', 'imagefield', 'imagefield_widget');

/**
 * Implements hook_elements().
 */
function fine_uploader_elements() {
  $elements = array();
  $elements_imagefield = module_invoke('imagefield', 'elements');
  $elements['fine_uploader_widget'] = $elements_imagefield['imagefield_widget'];

  return $elements;
}

/**
 * Implements CCK's hook_widget_info().
 */
function fine_uploader_widget_info() {
  return array(
    'fine_uploader_widget' => array(
      'label'           => t('Fine Uploader'),
      'field types'     => array('image', 'filefield'),
      'multiple values' => CONTENT_HANDLE_CORE,
      'callbacks'       => array('default value' => CONTENT_CALLBACK_CUSTOM),
      'description'     => t('Widget for image files being uploaded via Fine Uploader.'),
    ),
  );
}

/**
 * Implements CCK's hook_widget_settings().
 */
function fine_uploader_widget_settings($op, $widget) {

  switch ($op) {
    case 'form':
      return fine_uploader_widget_settings_form($widget);
    case 'validate':
      return fine_uploader_widget_settings_validate($widget);
    case 'save':
      return fine_uploader_widget_settings_save($widget);
  }
}

/**
 * Implements hook_widget().
 *
 * Assign default properties to item and delegate to filefield.
 */
function fine_uploader_widget(&$form, &$form_state, $field, $items, $delta = 0) {
  $element = imagefield_widget($form, $form_state, $field, $items, $delta);
  return $element;
}

/**
 * Implements CCK's hook_default_value().
 */
function fine_uploader_default_value(&$form, &$form_state, $field, $delta) {
  return filefield_default_value($form, $form_state, $field, $delta);
}

/**
 * CCK ImageFields.
 */
function fine_uploader_content_fieldapi($op, $field, $new_instance = NULL) {
  $image_node_types = variable_get('image_node_types', array());
  switch ($op) {
    case "delete instance":
      if ($image_node_types[$field['type_name']]['fieldname'] == $field['field_name']) {
        unset($image_node_types[$field['type_name']]);
        variable_set('image_node_types', $image_node_types);
      }
      break;
  }
}

/**
 * Documentation uncomplete.
 * @todo See if we need this function.
 */
function fine_uploader_form_fields_destroy(&$form, $fields_to_remove = array()) {
  // Collect information about fields being used by image preview list,
  // if needed.
  $fields = array();

  if (count($fields_to_remove)) {
    foreach ($fields_to_remove as $key) {
      $elements = explode("_", $key, 2);
      if (!isset($fields[$elements[0]]) && !empty($fields_to_remove[$key])) {
        $fields[$elements[0]] = array();
      }
      if (!empty($fields_to_remove[$key])) {
        array_push($fields[$elements[0]], $elements[1]);
      }
    }
  }

  // Any node specific fields which should be removed ..argg.
  if (isset($fields['node'])) {
    if (in_array("title", $fields['node'])) {
      unset($form['title']);
    }
    if (in_array("description", $fields['node'])) {
      unset($form['body_field']);
    }
  }

  if (isset($fields['taxonomy'])) {
    // Remove taxonomy items if necessary.
    for ($i = 0; $i < count($fields['taxonomy']); $i++) {
      unset($form['taxonomy'][$fields['taxonomy'][$i]]);
      unset($form['taxonomy']['tags'][$fields['taxonomy'][$i]]);
      if (!count($form['taxonomy']['tags'])) {
        unset($form['taxonomy']['tags']);
      }
    }
    // Still anything left to render?
    if (count(taxonomy_get_vocabularies($form['#node']->type)) == count($fields['taxonomy'])) {
      unset($form['taxonomy']);
    }
  }

  if (isset($fields['cck'])) {
    // Remove cck fields if necessary.
    for ($i = 0; $i < count($fields['cck']); $i++) {
      unset($form[$fields['cck'][$i]]);
      unset($form['#field_info'][$fields['cck'][$i]]);
    }
    if (!count($form['#field_info'])) {
      unset($form['#field_info']);
    }
  }
}

/**
 * FormAPI theme function. Theme the output of an image field.
 */
function theme_fine_uploader_widget($element) {
  return theme('form_element', $element, $element['#children']);
}

/**
 * Themeable function.
 * @ingroup themeable
 */
function theme_fine_upolader_item($item) {
  return theme('imagefield_item', $item);
}

/**
 * Themeable function.
 * @ingroup themeable
 */
function theme_image_fine_uploader_widget_preview($item = NULL) {
  return theme('imagefield_widget_preview', $item);
}

/**
 * Themeable function.
 * @ingroup themeable
 */
function theme_image_fine_uploader_widget_item($element) {
  return theme('imagefield_widget_item', $element);
}

/**
 * Implements hook_widget_settings().
 */
function fine_uploader_widget_settings_form($widget) {
  $form = module_invoke('imagefield', 'widget_settings_form', $widget);

  // AutoUpload.
  $form['fine_uploader_autoupload'] = array(
    '#type'          => 'radios',
    '#title'         => t('Auto Upload'),
    '#description'   => t('Allow to insta upload the files'),
    '#default_value' => !empty($widget['fine_uploader_autoupload']) ? $widget['fine_uploader_autoupload'] : 0,
    '#options'       => array(
      '0' => t('Disabled'),
      '1' => t('Enabled'),
    ),
  );

  return $form;
}

/**
 * Implements hook_widget_settings_validate().
 */
function fine_uploader_widget_settings_validate($widget) {
  // I'm still thinking about some validation here.
  return array();
}

/**
 * Implements hook_widget_settings().
 */
function fine_uploader_widget_settings_save($widget) {
  $imagefield_settings = module_invoke('imagefield', 'widget_settings_save', $widget);

  return array_merge($imagefield_settings, array('fine_uploader_autoupload'));
}
