<?php

/**
 * @file
 * Admin settings
 */

/**
 * Basic admin settings.
 */
function fine_uploader_settings() {
  $form = array();

  $form['fine_uploader_dev_js'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Development version'),
    '#description'   => t('Development version. This mean will be used the large file <em>fineuploader.js</em>. We recomend put off in deploy site.'),
    '#default_value' => variable_get('fine_uploader_dev_js', 0),
  );

  return system_settings_form($form);
}
