<?php

/**
 * @file
 * Internals function to handle the image
 */

/**
 * Internal process.
 * @return json
 *   json status
 */
function fine_uploader_process() {
  global $user;

  if (!empty($_GET['type']) && $_GET['type']) {
    $node_type = check_plain($_GET['type']);
  }
  if (!empty($_GET['fieldname']) && $_GET['fieldname']) {
    $field_name = check_plain($_GET['fieldname']);
  }
  if (!empty($_GET['qqfile']) && $_GET['qqfile']) {
    $file_name = check_plain($_GET['qqfile']);
  }

  // Now we have all the content needed. We can continue.
  $file_zise = (int) $_SERVER["CONTENT_LENGTH"];
  if ($file_zise == 0) {
    // We have a problem.
  }

  if (module_exists('content')) {
    if (image_node_type_load($node_type, TRUE) && ($field = content_fields($field_name, $node_type))) {
      $field_access = node_access('create', $node_type, $user);

      // Also get suitable validators for our upload: cck imagefield module.
      $validators = array_merge(filefield_widget_upload_validators($field), imagefield_widget_upload_validators($field));
    }
  }

  // Adapt to drupal files structure.
  $_FILES['files']['name']['image']     = $_FILES['qqfile']['name'];
  $_FILES['files']['type']['image']     = $_FILES['qqfile']['type'];
  $_FILES['files']['tmp_name']['image'] = $_FILES['qqfile']['tmp_name'];
  $_FILES['files']['error']['image']    = $_FILES['qqfile']['error'];
  $_FILES['files']['size']['image']     = $_FILES['qqfile']['size'];

  if (module_exists('transliteration')) {
    module_load_include('inc', 'transliteration', 'transliteration');
    $_FILES['files']['name']['image'] = transliteration_clean_filename($_FILES['qqfile']['name']);
  }

  // Correct widget directory.
  $widget_file_path = $field['widget']['file_path'];

  if (module_exists('token')) {
    $widget_file_path = token_replace($widget_file_path, 'user', $user);
  }

  $dir = file_directory_path() . '/' . $widget_file_path;
  if (!file_check_directory($dir, TRUE)) {
    $dir = file_directory_path();
  }

  if ($file = file_save_upload('image', $validators, $dir)) {
    print '{"success":true,"fid":' . $file->fid . '}';
    return;
  }
  else {
    // We have to show the correct message that the image can't be saved.
  }

  // Something goes wrong than we don't detect.
  print '{"success":false}';
}

/**
 * Rebuild form. I copy the add more field from core but removing the + 1 field.
 */
function fine_uploader_update_form($type_name_url, $field_name) {
  $content_type = content_types($type_name_url);
  $type = content_types($type_name_url);
  $field = content_fields($field_name, $type['type']);

  if (($field['multiple'] != 1) || empty($_POST['form_build_id'])) {
    // Invalid request.
    drupal_json(array('data' => ''));
    exit;
  }

  // Retrieve the cached form.
  $form_state = array('submitted' => FALSE);
  $form_build_id = check_plain($_POST['form_build_id']);
  $form = form_get_cache($form_build_id, $form_state);
  if (!$form) {
    // Invalid form_build_id.
    drupal_json(array('data' => ''));
    exit;
  }

  // We don't simply return a new empty widget to append to existing ones,
  // because
  // - ahah.js won't simply let us add a new row to a table
  // - attaching the 'draggable' behavior won't be easy
  // So we resort to rebuilding the whole table of widgets including the
  // existing ones,
  // which makes us jump through a few hoops.
  // The form that we get from the cache is unbuilt. We need to build it so,
  // that _value callbacks can be executed and $form_state['values'] populated.
  // We only want to affect $form_state['values'], not the $form itself,
  // (built forms aren't supposed to enter the cache) nor the rest of,
  // $form_data,
  // so we use copies of $form and $form_data.
  $form_copy = $form;
  $form_state_copy = $form_state;
  $form_copy['#post'] = array();
  form_builder(check_plain($_POST['form_id']), $form_copy, $form_state_copy);
  // Just grab the data we need.
  $form_state['values'] = $form_state_copy['values'];
  // Reset cached ids, so that they don't affect the actual form we output.
  form_clean_id(NULL, TRUE);

  // Sort the $form_state['values'] we just built *and* the incoming $_POST,
  // data.
  // According to d-n-d reordering.
  unset($form_state['values'][$field_name][$field['field_name'] . '_add_more']);
  foreach ($_POST[$field_name] as $delta => $item) {
    $form_state['values'][$field_name][$delta]['_weight'] = $item['_weight'];
    $form_state['values'][$field_name][$delta]['_remove'] = isset($item['_remove']) ? $item['_remove'] : 0;
  }
  $form_state['values'][$field_name] = _content_sort_items($field, $form_state['values'][$field_name]);
  $_POST[$field_name] = _content_sort_items($field, $_POST[$field_name]);

  // Build our new form element for the whole field, asking for one more,
  // element.
  $delta = max(array_keys(check_plain($_POST[$field_name])));
  $form_state['item_count'] = array($field_name => count(check_plain($_POST[$field_name])));
  $form_element = content_field_form($form, $form_state, $field);
  // Let other modules alter it.
  // We pass an empty array as hook_form_alter's usual 'form_state' parameter,
  // instead of $form_atate (for reasons we may never remember).
  // However, this argument is still expected to be passed by-reference,
  // (and PHP5.3 will throw an error if it isn't.) This leads to:
  $data = &$form_element;
  $empty_form_state = array();
  $data['__drupal_alter_by_ref'] = array(&$empty_form_state);
  drupal_alter('form', $data, 'content_add_more_js');

  // Add the new element at the right place in the (original, unbuilt) form.
  $success = content_set_nested_elements($form, $field_name, $form_element[$field_name]);

  // Save the new definition of the form.
  $form_state['values'] = array();
  form_set_cache($form_build_id, $form, $form_state);

  // Build the new form against the incoming $_POST values so that we can
  // render the new element.
  $_POST[$field_name][$delta]['_weight'] = $delta;
  $form_state = array('submitted' => FALSE);
  $form += array(
    '#post'       => $_POST,
    '#programmed' => FALSE,
  );
  $form = form_builder(check_plain($_POST['form_id']), $form, $form_state);

  // Render the new output.
  $field_form = array_shift(content_get_nested_elements($form, $field_name));

  // We add a div around the new content to receive the ahah effect.
  $field_form[$delta]['#prefix'] = '<div class="ahah-new-content">' . (isset($field_form[$delta]['#prefix']) ? $field_form[$delta]['#prefix'] : '');
  $field_form[$delta]['#suffix'] = (isset($field_form[$delta]['#suffix']) ? $field_form[$delta]['#suffix'] : '') . '</div>';
  // Prevent duplicate wrapper.
  unset($field_form['#prefix'], $field_form['#suffix']);

  // If a newly inserted widget contains AHAH behaviors, they normally won't
  // work because AHAH doesn't know about those - it just attaches to the exact
  // form elements that were initially specified in the Drupal.settings object.
  // The new ones didn't exist then, so we need to update Drupal.settings
  // by ourselves in order to let AHAH know about those new form elements.
  $javascript = drupal_add_js(NULL, NULL);
  $output_js = isset($javascript['setting']) ? '<script type="text/javascript">jQuery.extend(Drupal.settings, ' . drupal_to_js(call_user_func_array('array_merge_recursive', $javascript['setting'])) . ');</script>' : '';

  $output = theme('status_messages') . drupal_render($field_form) . $output_js;

  // Using drupal_json() breaks filefield's file upload, because the jQuery
  // Form plugin handles file uploads in a way that is not compatible with
  // 'text/javascript' response type.
  $GLOBALS['devel_shutdown'] = FALSE;
  print drupal_to_js(array('status' => TRUE, 'data' => $output));
  exit;
}
