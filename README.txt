# Fine Uploader

Allow you upload mass files at once. Integration with the library
[fine uploader](http://fineuploader.com/ "a.k.a valums file-uploader")
Browser support (as say the developer) IE7+, Firefox, Safari (OS X), Chrome.
It does not use *Flash*, *jQuery* or *any external libraries*
(we need only the fine uploader library).

## How it works

Uses an XMLHttpRequest(AJAX) for uploading multiple files with a progress-bar.

## Requiered

+ [fine uploader](http://fineuploader.com/#setup "a.k.a valums file-uploader")
+ [cck](http://drupal.org/project/cck "content")
+ [filefield](http://drupal.org/project/filefield "filefield")
+ [imagefield](http://drupal.org/project/imagefield "imagefield")
+ [imagecache *optional](http://drupal.org/project/imagecache "imagecache")

## Installation.

+ Download the last [fine uploader](http://fineuploader.com/#setup "a.k.a valums
 file-uploader") release
+ Place it into *sites/all/libraries/fine_uploader* folder
+ Download and active this module
+ Setup permision in *admin/user/permissions*
+ Go into some content type and edit a field with image option and select the
widget **fine uploader**
+ And its done upload some files and try it.

## Credits

Created & maintained by
[Luigi Guevara](http://drupal.org/user/699418 "killua99")
Fine Uploader development is sponsored by
[idealista.com](http://www.idealista.com/
"el portal inmobiliario líder en españa")
